package acad276.choe.ben.a3;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {
    EditText billTotal;
    TextView total;
    EditText tipPercent;
    TextView tip;
    private Button calculator;
    EditText split;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        billTotal = (EditText) findViewById(R.id.billTotal);
        total = (TextView) findViewById(R.id.total);
        tipPercent = (EditText) findViewById(R.id.tipPercent);
        tip = (TextView) findViewById(R.id.tip);
        calculator = (Button) findViewById(R.id.calculator);
        split = (EditText) findViewById(R.id.split);

        calculator.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                double bill = Double.parseDouble(billTotal.getText().toString());
                double percent = Double.parseDouble(tipPercent.getText().toString());
                double checkSplit = Double.parseDouble(split.getText().toString());
                double checkTotal;
                double finalTip;
                percent = percent / 100;
                checkTotal = (bill + (bill * percent))/checkSplit;
                checkTotal = Math.floor(checkTotal*100) / 100;
                finalTip = Math.floor((bill * percent) * 100) / 100;
                total.setText("$ " + checkTotal + " per person");
                tip.setText("$" + finalTip);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
