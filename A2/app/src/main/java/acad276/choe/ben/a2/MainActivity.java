package acad276.choe.ben.a2;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;


public class MainActivity extends Activity {
    ImageView[] design;

    int acCount;
    int adCount;
    int rCount;
    int mCount;
    int paCount;
    int bCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final String[] designEras = getResources().getStringArray(R.array.designEras);
        final String[] designURLS = getResources().getStringArray(R.array.designURLS);

        acCount = 0;
        adCount = 0;
        rCount = 0;
        mCount = 0;
        paCount = 0;
        bCount = 0;

        design = new ImageView[6];
        design[0] = (ImageView) findViewById(R.id.designURLS0);
        design[1] = (ImageView) findViewById(R.id.designURLS1);
        design[2] = (ImageView) findViewById(R.id.designURLS2);
        design[3] = (ImageView) findViewById(R.id.designURLS3);
        design[4] = (ImageView) findViewById(R.id.designURLS4);
        design[5] = (ImageView) findViewById(R.id.designURLS5);

        TextView textView = (TextView)findViewById(R.id.textView);
        textView.setText(designEras[0]);
        TextView textView2 = (TextView)findViewById(R.id.textView2);
        textView2.setText(designEras[1]);
        TextView textView3 = (TextView)findViewById(R.id.textView3);
        textView3.setText(designEras[2]);
        TextView textView4 = (TextView)findViewById(R.id.textView4);
        textView4.setText(designEras[3]);
        TextView textView5 = (TextView)findViewById(R.id.textView5);
        textView5.setText(designEras[4]);
        TextView textView6 = (TextView)findViewById(R.id.textView6);
        textView6.setText(designEras[5]);

            for (int i = 0; i < design.length; i++) {
                Glide.with(getApplicationContext())
                        .load(designURLS[i])
                        .into(design[i]);
            }


        design[0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acCount += 1;
                Toast.makeText(getApplicationContext(),
                        "You liked " + designEras[0] + " " + acCount + " times.", Toast.LENGTH_SHORT).show();
            }
        });

        design[1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adCount += 1;
                Toast.makeText(getApplicationContext(),
                        "You liked " + designEras[1] + " " + adCount + " times.", Toast.LENGTH_SHORT).show();
            }
        });

        design[2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rCount += 1;
                Toast.makeText(getApplicationContext(),
                        "You liked " + designEras[2] + " " + rCount + " times.", Toast.LENGTH_SHORT).show();
            }
        });

        design[3].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCount += 1;
                Toast.makeText(getApplicationContext(),
                        "You liked " + designEras[3] + " " + mCount + " times.", Toast.LENGTH_SHORT).show();
            }
        });

        design[4].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paCount += 1;
                Toast.makeText(getApplicationContext(),
                        "You liked " + designEras[4] + " " + paCount + " times.", Toast.LENGTH_SHORT).show();
            }
        });

        design[5].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bCount += 1;
                Toast.makeText(getApplicationContext(),
                        "You liked " + designEras[5] + " " + bCount + " times.", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
