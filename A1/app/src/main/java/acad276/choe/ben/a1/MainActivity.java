package acad276.choe.ben.a1;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;
import android.view.View;

public class MainActivity extends Activity {

    private Button Skittles;
    private Button Starburst;
    public int SKITTLE_COUNT = 0;
    public int STARBURST_COUNT = 0;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            Skittles = (Button) findViewById(R.id.Skittles);
            Skittles.setOnClickListener(new View.OnClickListener() {
                @Override
            public void onClick (View v) {
                    SKITTLE_COUNT++;
                    Toast.makeText(getApplicationContext(), "You ordered some dank skittles! (" + SKITTLE_COUNT + " time[s])", Toast.LENGTH_SHORT).show();
                    }
                });

            Starburst = (Button) findViewById(R.id.Starburst);
            Starburst.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v){
                    STARBURST_COUNT++;
                    Toast.makeText(getApplicationContext(), "You ordered some fantastical starburst! (" + STARBURST_COUNT + " time[s])", Toast.LENGTH_SHORT).show();
                }
            });
        }

    protected void onSaveInstanceState (Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("counterSkittle", SKITTLE_COUNT);

        super.onSaveInstanceState(outState);
        outState.putInt("counterStarBurst", STARBURST_COUNT);
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        SKITTLE_COUNT=savedInstanceState.getInt("counterSkittle");

        super.onRestoreInstanceState(savedInstanceState);
        STARBURST_COUNT=savedInstanceState.getInt("counterStarburst");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}